<h1 align="center"> Teste Miguel Moscardini – Irroba</h1>

<p align="center">Construção de um CRUD, onde é criado uma API que consumirá uma API externa favorecida pela Irroba, onde fará as rotinas de uma API normalmente.</p>

<h4 align="center"> 
        🚧  Teste Miguel Moscardini - Irroba 🚀 Finalizado.  🚧
</h4>
### Pré-requisitos
Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[PHP ^7](https://www.php.net), [MySql](https://www.mysql.com), [Laravel ^8](https://laravel.com/).

### 🎲 Rodando a API

```bash
# Clone este repositório
$ git clone https://gitlab.com/miguelbatarra123/api_irroba.git

# Acesse a pasta do projeto no terminal/cmd
$ cd api_irroba

# Dê as permissões as pastas
$ sudo chmod -R 775 ../api_irroba
$ sudo chown -R [usuario:grupo] ../api_irroba

# Acesse o arquivo por algum editor
$ code . (VSCode).

# Copie o arquivo .env example, altere seu nome para .env e altere somente os dados do banco de dados referente ao que você irá utilizar.

# Atualize as dependências 
$ composer update

# Adicione as tabelas criadas ao DB
$ php artisan migrate

# Crie o usuário a API
$ php artisan db:seed 

#Execute a aplicação na porta 8000
$ php artisan serve --port=8000

# O servidor inciará na porta:8000 - acesse <http://127.0.0.1:8000>

# Login de acesso a API:
# {
#   "name": "user_api",
#   "password": "123456&rroba"
# }

# O token gerado deve ser inserido no Bearer
```
### 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [Laravel](https://www.laravel.com)
- [Mysql](https://www.mysql.com)
- [Bootstrap](https://getbootstrap.com)
- [JWT](https://jwt-auth.readthedocs.io/en/develop/)
<h1 align="center"> Autor </h1>
<p>Feito por Miguel Moscardini Battarra 👋🏽</p>

MIT License

Copyright (c) <2022> Miguel Moscardini Battarra

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

