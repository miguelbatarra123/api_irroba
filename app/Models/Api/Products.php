<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
        'product_id',
        'name',
        'price',
        'sku'
    ];


    public function productCategory()
    {
        return $this->hasMany('App\Models\Api\CategoryProduct', 'product_id', 'product_id');
    }


}
