<?php

namespace App\Services\Api;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

use App\Models\Api\CategoryProduct;


class InsertCategoryProductService
{

    
    public function __contruct(CategoryProduct $categoryProduct){

        $this->categoryProduct = $categoryProduct;

    }


    public function getCategoryProduct(){

        return CategoryProduct::all();

    }
    
    public function insertCategoryProduct($request){

        $categoryProduct = CategoryProduct::create([
            'category_id' => $request->category_id,
	        'product_id' => $request->product_id
        ]);

        return $categoryProduct;

    }


    public function getCategoryProductId($id){

        return CategoryProduct::findOrFail($id);

    }


    public function updateCategoryProduct(){

        return response()->json(['Error' => 'invalid Method']);

    }


    public function removeCategoryProduct($request, $id){

        $categoryProduct = CategoryProduct::findOrFail($id);
        $categoryProduct->delete($request->all());

    }


}
