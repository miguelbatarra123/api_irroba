<?php

namespace App\Services\Api;

use Illuminate\Support\Facades\Http;


class GetProductsService
{
    public function getProductApi($id)
    {
        $response = Http::post(env('BASE_API') . '/getToken', [
            'username' => env('API_USER'),
            'password' => env('API_PASSWORD')
        ]);

        $key = $response->json()['data']['authorization'];

        $route = $id ? $route = '/product/' . $id : '/product';

        $response = Http::withHeaders(['Authorization' => $key])->get(env('BASE_API') . $route . '?page=1&limit=25&sort=date_added&order=DESC');

        // $response->unauthorized();
        
        return $response;
    }

}
