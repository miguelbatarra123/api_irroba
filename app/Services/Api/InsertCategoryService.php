<?php

namespace App\Services\Api; 

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

use App\Models\Api\Category;


class InsertCategoryService
{
    public function __contruct(Category $category){

        $this->category = $category;
        
    }


    public function getCategory(){

        return Category::all();
        
    }
    
    
    public function insertCategory($request){

        $category = Category::create([
            'category_id' => $request->category_id,
            'category_name' => $request->category_name
        ]);

        return $category;

    }

    
    public function getCategoryId($id){
        
        return Category::findOrFail($id);

    }


    public function updateCategory(){

        return response()->json(['Error' => 'invalid Method']);

    }


    public function removeCategory($request, $id){

        $category = Category::findOrFail($id);
        $category->delete($request->all());

    }

}
