<?php

namespace App\Services\Api; 

use Illuminate\Support\Facades\Validator;

use App\Models\Api\Products;


class InsertProductService
{

    
    public function __contruct(Products $products){

        $this->products = $products;

    }


    public function getProduct(){

        return Products::with('productCategory')->get();

    }

    
    public function insertProduct($request){
        
        $validate = $this->validateProduct($request);

        if (!$validate->messages()){

            $product = Products::create([
                'product_id' => $request->product_id,
                'name' => $request->name,
                'price' => $request->price,
                'sku' => $request->sku
            ]);
    
            return $product;

        } else{

            return $validate;

        }


    }


    public function getProductId($id){

        return Products::with('productCategory')->where('product_id', $id)->first();

    }


    public function updateProduct($request, $id){

        $product = Products::where('product_id', $id);
        $product->update($request->all());
        return Products::where('product_id', $id)->first();

    }


    public function removeProduct($request, $id){
 
        $product = Products::where('product_id', $id);
        $product->delete($request->all());

    }


    private function validateProduct($request){

        $attributes = [

            'product_id' => 'ID do produto'

        ];

        $messages = [

            'required' => 'O campo :attribute é obrigatório.',
            'numeric'  => ':attibute deve ser numérico.',
            'unique'   => 'O produto ja com id :attribute ja esta inserido!'

        ];

        $validate = Validator::make($request->all(), [

            'product_id' => 'required|numeric|unique:App\Models\Api\Products,product_id'

        ], $messages, $attributes);


        return $validate->errors();

    }

}
