<?php

namespace App\Http\Middleware;

use App\Services\Api\TokenService;
use Illuminate\Support\Facades\Auth;

use Closure;

class Token
{

    public function handle($request, Closure $next)
    {
        if (!Auth::check()){

            $request['Error'] = 'Invalid Token';
            return response()->json($request);
            
        } else {
            
            return $next($request);
        }


    }
}
