<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Services\Api\GetProductsService;


class GetProductsController extends Controller
{
   
    public function __construct(GetProductsService $getProductsService)
    {
        $this->getProductsService = $getProductsService;
    }


    public function getProductApi($id = null, Request $request)
    {
        // dd($request->all());
        return $this->getProductsService->getProductApi($id);
    }
}
