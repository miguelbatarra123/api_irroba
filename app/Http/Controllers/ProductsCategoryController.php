<?php

namespace App\Http\Controllers; 

use Illuminate\Http\Request;

use App\Services\Api\InsertCategoryProductService;

class ProductsCategoryController extends Controller
{
    public function __construct(InsertCategoryProductService $insertCategoryProductService) 
    {
        $this->insertCategoryProductService = $insertCategoryProductService;
    }


    public function index()
    {
        return $this->insertCategoryProductService->getCategoryProduct();
    }

   
    public function store(Request $request)
    {
        return $this->insertCategoryProductService->insertCategoryProduct($request);
    }

    
    public function show($id)
    {
        return $this->insertCategoryProductService->getCategoryProductId($id);
    }


    public function update(Request $request, $id)
    {
        return $this->insertCategoryProductService->updateCategoryProduct();
    }

    
    public function destroy(Request $request, $id)
    {
        return $this->insertCategoryProductService->removeCategoryProduct($request, $id);
    }

}
