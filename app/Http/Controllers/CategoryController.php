<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\Api\InsertCategoryService;

class CategoryController extends Controller
{
    public function __construct(InsertCategoryService $insertCategoryService)
    {
        $this->insertCategoryService = $insertCategoryService;
    }


    public function index()
    {
        return $this->insertCategoryService->getCategory();
    }

   
    public function store(Request $request)
    {
        return $this->insertCategoryService->insertCategory($request);
    }

    
    public function show($id)
    {
        return $this->insertCategoryService->getCategoryId($id);
    }


    public function update(Request $request, $id)
    {
        return $this->insertCategoryService->updateCategory();
    }

    
    public function destroy(Request $request, $id)
    {
        return $this->insertCategoryService->removeCategory($request, $id);
    }

}
