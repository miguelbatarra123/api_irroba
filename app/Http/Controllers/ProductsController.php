<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\Api\InsertProductService; 

class ProductsController extends Controller
{
    public function __construct(InsertProductService $insertProductService)
    {
        $this->insertProductService = $insertProductService;
    }


    public function index()
    {
        return $this->insertProductService->getProduct();
    }

   
    public function store(Request $request)
    {
        return $this->insertProductService->insertProduct($request);
    }

    
    public function show($id)
    {
        return $this->insertProductService->getProductId($id);
    }


    public function update(Request $request, $id)
    {
        return $this->insertProductService->updateProduct($request, $id);
    }

    
    public function destroy(Request $request, $id)
    {
        return $this->insertProductService->removeProduct($request, $id);
    }

}
