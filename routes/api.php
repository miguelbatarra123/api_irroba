<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function(){
    return response()->json(['api_name' => 'API teste Irroba', 'Api_version' => '1.0.0']);
});


Route::post('login', 'AuthController@login');

Route::group(['middleware' => 'token'], function($router){
    
    // Rotas do JWT
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

    // Rotas API Externa
    Route::post('apiExternal/{id?}', 'GetProductsController@getProductApi');

    // Rotas AOI Interna
    Route::prefix('apiInternal')->group( function(){
        
        Route::apiResource('product', 'ProductsController');
        Route::apiResource('category', 'CategoryController');
        Route::apiResource('product_category', 'ProductsCategoryController');
    
    });

});