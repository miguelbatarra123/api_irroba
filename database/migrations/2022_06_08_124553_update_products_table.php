<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProductsTable extends Migration
{
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->text('name')->nullable()->change();
            $table->float('price', 6, 2)->nullable()->change();
            $table->text('sku')->nullable()->change();
        });
        
    }

    public function down()
    {
        //
    }
}
